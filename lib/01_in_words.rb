# extension to class fixnum to output in words
class Fixnum
  def in_words
    case
    when self >= 1000000000000
      country_wealth(self)
    when self >= 1000000000
      ten_figures(self)
    when self >= 1000000
      seven_figures(self)
    when self > 999
      four_digits(self)
    when self > 99
      triple_digits(self)
    when self > 9
      double_digits(self)
    else
      single_digit(self)
    end
  end

  def country_wealth(num)
    trillions = (num / 1000000000000).in_words + ' trillion'
    the_rest = num % 1000000000000
    the_rest.zero? ? trillions : trillions + " #{the_rest.in_words}"
  end

  def ten_figures(num)
    billions = (num / 1000000000).in_words + ' billion'
    the_rest = num % 1000000000
    the_rest.zero? ? billions : billions + " #{the_rest.in_words}"
  end

  def seven_figures(num)
    millions = (num / 1000000).in_words + ' million'
    the_rest = num % 1000000
    the_rest.zero? ? millions : millions + " #{the_rest.in_words}"
  end

  def four_digits(num)
    thousands = (num / 1000).in_words + ' thousand'
    the_rest = num % 1000
    the_rest.zero? ? thousands : thousands + " #{the_rest.in_words}"
  end

  def triple_digits(num)
    hundreds = single_digit(num / 100) + ' hundred'
    the_rest = num % 100
    the_rest.zero? ? hundreds : hundreds + " #{the_rest.in_words}"
  end

  def double_digits(num)
    tens = num / 10
    return teens(num) if tens == 1
    ones = num % 10
    prefix = case tens
    when 2
      'twenty'
    when 3
      'thirty'
    when 4
      'forty'
    when 5
      'fifty'
    when 6
      'sixty'
    when 7
      'seventy'
    when 8
      'eighty'
    when 9
      'ninety'
    end
    ones.zero? ? prefix : prefix + " #{single_digit(num % 10)}"
  end

  def teens(num)
    case num
    when 10
      'ten'
    when 11
      'eleven'
    when 12
      'twelve'
    when 13
      'thirteen'
    when 15
      'fifteen'
    when 18
      'eighteen'
    else
      single_digit(num % 10) + 'teen'
    end
  end

  def single_digit(num)
    case num
    when 0
      'zero'
    when 1
      'one'
    when 2
      'two'
    when 3
      'three'
    when 4
      'four'
    when 5
      'five'
    when 6
      'six'
    when 7
      'seven'
    when 8
      'eight'
    when 9
      'nine'
    end

  end
end
